// ------------------------------------------------------------------------- //
// ------------------------------------------------------------------------- //
//
// irrelevant.irreverent's bullet spread functions
//
// author irrelevant.irreverent
// version 1.0.1
//
// --------------------------------------------------------------------- //
// --------------------------------------------------------------------- //
//
// to use: place in your weapon add-on with an exec in server.cs or elsewhere
// and call spreadProjectile/spreadRaycast as appropriate
//
// these two functions, as well as findSpread, will return tab-delimited vector
// lists for you to use in other places in your code, if you'd like to
//
// optionally: define any or all of the following properties on your weapon
// image datablock, as described, so your function calls need fewer arguments
//
//     spreadRadius: radius of spread
//     spreadAimDistance: distance at which spread reaches the specified radius
//     spreadCenter: 0-0.5: spread avoids center, 0.5+: spread tends to center
//     perShotCount: how many projectiles or rays to fire
//
//     spreadProjectile: projectile to fire from projectile spread method
//
//     rayMaxDistance: how far the ray can go, at most
//     rayProjectile: what projectile the ray should act like (explosions, etc)
//     rayCosmeticProjectile: what projectile the gun shoots normally (tracer)
//     rayDroppedProjectile: what projectile the ray drops at the end point
//     rayTypeMasks: what typemasks to strike with the ray
//
//     fromEye: whether to fire from eye position, same as setting melee flag
//
// anything defined by property like this can be omitted or replaced with an
// empty string, like the following example call:
//
//     %this.spreadRaycast(%player, %slot, "", 500, 3);
//
// in this example, spread parameters are supplied from property, and maximum
// distance and ray count are supplied from argument
//
// --------------------------------------------------------------------- //
// --------------------------------------------------------------------- //
//
// findSpread
//
// returns a direction vector given several parameters
//
// this the weapon
// player the player
// slot the image slot the weapon is being fired from
//
// spreadParams vector containing, in this order:
// 0) radius the radius of spread at distance aimDist
// 1) aimDist the distance at which the maximum spread will reach radius
// 2) center the central tendency of the spread:
//   0: exactly on the circle of spread, forming a ring
//   0-0.5: tendency towards side, forming a ring
//   0.5: no tendency, even spread over the disk
//   0.5+: tendency towards center, the larger the greater
//
// speedOrDist the speed of the projectile or the distance a raycast can travel
//
// -------- //
//
function WeaponImage::findSpread(%this, %player, %slot, %spreadParams, %speedOrDist) {
    %aimDir = (%this.fromEye || %this.melee) ? %player.getEyeVector() : %player.getMuzzleVector(%slot);

    %radius =  (getWord(%spreadParams, 0) !$= "") ? getWord(%spreadParams, 0) : %this.spreadRadius;

    if (%radius == 0) {
        return vectorScale(vectorNormalize(%aimDir), %speedOrDist);
    }

    %aimDist = (getWord(%spreadParams, 1) !$= "") ? getWord(%spreadParams, 2) : %this.spreadAimDistance;
    %center =  (getWord(%spreadParams, 2) !$= "") ? getWord(%spreadParams, 1) : %this.spreadCenter;

    %localHoriz = vectorCross("0 0 1", %aimDir);
    %localVert = vectorCross(%localHoriz, %aimDir);

    %r = mPow(getRandom(), %center) * %radius;  // get a random distance within the radius, with central tendency bias
    %t = 2 * 3.1415926536 * getRandom();
    %x = %r * mCos(%t);
    %y = %r * mSin(%t);

    %aimVec = vectorScale(%aimDir, %aimDist);
    %horizSpread = vectorScale(%localHoriz, %x);
    %vertSpread = vectorScale(%localVert, %y);

    %spreadDir = vectorNormalize(vectorAdd(%aimVec,
                                           vectorAdd(%horizSpread,
                                                     %vertSpread)));

    %spreadVec = vectorScale(%spreadDir, %speedOrDist);

    return %spreadVec;
}
//
// --------------------------------------------------------------------- //
// --------------------------------------------------------------------- //
//
// spreadProjectile
//
// spawns projectile(s) in a spread given several parameters
//
// returns a tab-delimited list of each vector used
//
// this as with findSpread
// player as with findSpread
// slot as with findSpread
// spreadParams as with findSpread
// count number of projectiles to spawn
// proj optional projectile datablock to spawn, uses weapon's if not provided
//
// -------- //
//
function WeaponImage::spreadProjectile(%this, %player, %slot, %spreadParams, %count, %proj) {
    %count = (%count !$= "") ? %count : %this.perShotCount;
    if (%count <= 0) {
        return;
    }

    // super long line - if we have a projectile passed through %proj, use that
    // otherwise, if we have a spreadprojectile field, use that
    // otherwise, use the projectile field
    %proj = (isObject(%proj) && %proj.getClassName() $= "ProjectileData") ? %proj : ((isObject(%this.spreadProjectile) && %this.spreadProjectile.getClassName() $= "ProjectileData") ? %this.spreadProjectile : %this.projectile);

    %fromPos = (%this.fromEye || %this.melee) ? getWords(%player.getEyeTransform(), 0, 2) : %player.getMuzzlePoint(%slot); // determine where we should spawn the projectile(s) from

    %vectorList = "";

    for (%i = 0; %i < %count; %i++) {
        %vel = %this.findSpread(%player, %slot, %spreadParams, %proj.muzzleVelocity);
        %p = new Projectile() {
            datablock = %proj;
            initialVelocity = %vel;
            initialPosition = %fromPos;
            sourceObject = %player;
            sourceSlot = %slot;
            client = %player.client;
        };
        MissionCleanup.add(%p); // add the projectile to MissionCleanup

        %vectorList = %vectorList TAB %vel;
    }
    return trim(%vectorList);
}
//
// --------------------------------------------------------------------- //
// --------------------------------------------------------------------- //
//
// spreadRaycast
//
// casts ray(s) in a spread given many parameters
//
// returns a tab-delimited list of each vector used
//
// this as with findSpread
// player as with findSpread
// slot as with findSpread
// spreadParams as with findSpread
// distance how far the ray should go, maximum
// count number of rays in one "shot"
//
// below are optional parameters dealing with the ray's behavior and appearance
//
// typeMasks typemasks to strike, has good defaults if this confuses you
// explosionProj a projectile datablock, the ray mimics this projectile
// fakeProj a projectile datablock, travels along the ray at its muzzleVelocity
// droppedProj projectile to simply spawn at the end of the ray, if any
//
// -------- //
//
function WeaponImage::spreadRaycast(%this, %player, %slot, %spreadParams, %distance, %count, %typeMasks, %explosionProj, %fakeProj, %droppedProj) {
    // set up variables based on properties and/or nonzero arguments passed
    %distance = (%distance !$= "") ? %distance : %this.rayMaxDistance;

    %count = (%count !$= "") ? %count : %this.perShotCount;
    if (%count <= 0) {
        return;
    }

    // similar to the prior %proj line in spreadProjectile
    %fakeProj = (isObject(%fakeProj) && %fakeProj.getClassName() $= "ProjectileData") ? %fakeProj : %this.rayCosmeticProjectile;

    %explosionProj = (isObject(%explosionProj) && %explosionProj.getClassName() $= "ProjectileData") ? %explosionProj : ((isObject(%this.rayProjectile && %this.rayProjectile.getClassName() $= "ProjectileData")) ? %this.rayProjectile : %this.projectile);

    %typeMasks = (%typeMasks) ? %typeMasks : ((%this.rayTypeMasks) ? %this.rayTypeMasks : ($TypeMasks::PlayerObjectType |
                                                                                           $TypeMasks::StaticObjectType |
                                                                                           $TypeMasks::TerrainObjectType |
                                                                                           $TypeMasks::VehicleObjectType |
                                                                                           $TypeMasks::FXBrickObjectType));
    %droppedProj = (isObject(%droppedProj)) ? %droppedProj : %this.rayDroppedProjectile;

    %fromPos = (%this.fromEye || %this.melee) ? getWords(%player.getEyeTransform(), 0, 2) : %player.getMuzzlePoint(%slot);

    %vectorList = "";

    for (%i = 0; %i < %count; %i++) {
        %vector = %this.findSpread(%player, %slot, %spreadParams, %distance);

        %droppedVelocity = vectorScale(vectorNormalize(%vector), %droppedProj.muzzleVelocity);
        %fakeVelocity = vectorScale(vectorNormalize(%vector), %fakeProj.muzzleVelocity);
        %velocity = vectorScale(vectorNormalize(%vector), %explosionProj.muzzleVelocity);

        %toPos = vectorAdd(%fromPos, %vector);

        %ray = containerRayCast(%fromPos, %toPos, %typeMasks, %player);

        %hit = getWord(%ray, 0);
        %hitPos = (isObject(%hit)) ? getWords(%ray, 1, 3) : %toPos;
        %hitNormal = (isObject(%hit)) ? getWords(%ray, 4, 6) : "0 0 0";

        if (isObject(%droppedProj)) {
            %dpp = new Projectile() {
                datablock = %droppedProj;
                initialVelocity = %droppedVelocity;
                initialPosition = %hitPos;
                sourceObject = %player;
                sourceSlot = %slot;
                client = %player.client;
            };
            MissionCleanup.add(%p);
        }

        if (isObject(%fakeProj)) {
            %cp = new Projectile() {
                datablock = %fakeProj;
                initialVelocity = %fakeVelocity;
                initialPosition = %fromPos;
                sourceObject = %player;
                sourceSlot = %slot;
                client = %player.client;
            };
            MissionCleanup.add(%cp);
        }

        if (isObject(%explosionProj) && isObject(%hit)) {
            %ep = new Projectile() {
                datablock = %explosionProj;
                initialVelocity = %velocity;
                initialPosition = %hitPos;
                sourceObject = %player;
                sourceSlot = %slot;
                client = %player.client;
            };
            %explosionProj.onCollision(%ep, %hit, 0, %hitPos, %hitNormal, %velocity);
            %ep.explode();
        }
        %vectorList = %vectorList TAB %vector;
    }
    return trim(%vectorList);
}
//
// ------------------------------------------------------------------------- //
// ------------------------------------------------------------------------- //
