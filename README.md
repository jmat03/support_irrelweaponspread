Note that this is NOT a C# file, but a TorqueScript file. It will not run as C#.
It will also likely not run in a standard TorqueScript environment, and even if
it does, it will not do anything on its own. Please use this as a Blockland-only
support script for other add-ons.